<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function index()
    {
        return view('home');
    }
	
	public function get_data()
    {
        $products = file_get_contents("products.json");
		$results = json_decode($products, true);
		
		$total = 0;
		
		foreach($results['products'] as $ind=>$row){
			echo "<tr>";
				echo "<td>".($ind+1)."</td>";
				echo "<td>".$row['product_name']."</td>";
				echo "<td>".$row['quantity']."</td>";
				echo "<td>".$row['price']."</td>";
				echo "<td>".$row['date_submitted']."</td>";
				echo "<td>".$row['quantity']*$row['price']."</td>";
			echo "</tr>";
			
			$total += $row['quantity']*$row['price'];
		}
		
		echo "<tr>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td class='text-right'><strong>Total</strong></td>";
				echo "<td>".$total."</td>";
			echo "</tr>";
		
		
    }
	
	public function store(Request $request)
    {
		$input = array();
		
		$products = file_get_contents("products.json");
		$results = json_decode($products, true);
		
		$input['product_name'] = $request->input('product_name');
		$input['quantity'] = $request->input('quantity');
		$input['price'] = $request->input('price');
		$input['date_submitted'] = date('Y-m-d H:i:s');
		
		$results['products'][] = $input;

		$fp = fopen('products.json', 'w');
		fwrite($fp, json_encode($results));
		fclose($fp);
    }
}