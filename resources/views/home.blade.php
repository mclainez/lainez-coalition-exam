<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
		
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		
		<link href='https://fonts.googleapis.com/css?family=Karla:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

    </head>
    <body>
        <div class="container">
			<div class="col-sm-6 col-sm-offset-3">
				<h1>Laravel Skill Test</h1>
				
				<form id="the_form" method="POST" action="/">
					<div class="form-group">
						<label for="exampleInputEmail1">Product Name</label>
						<input type="text" class="form-control" id="product_name" name="product_name" placeholder="Product Name">
					</div>
					
					<div class="form-group">
						<label for="exampleInputEmail1">Quantity in Stocks</label>
						<input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity">
					</div>
					
					<div class="form-group">
						<label for="exampleInputEmail1">Price</label>
						<input type="text" class="form-control" id="price" name="price" placeholder="Price per Item">
					</div>
				
					<button type="submit" class="btn btn-default">Add Product</button>
				</form>
			</div>
			
			<div class="col-sm-12">
				<h1>Products</h1>
				
				<table class="table table-bordered">
					<th>#</th>
					<th>Product name</th>
					<th>Quantity in Stocks</th>
					<th>Price per Item</th>
					<th>Date Submitted</th>
					<th>Total Value Number</th>
					<tbody id="table-data">
						
					</tbody>
				</table>
			</div>
		</div>
    </body>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="http://malsup.github.com/jquery.form.js"></script> 
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script>
		$(document).ready(function() { 
            $('#the_form').ajaxForm(function() {
				
				get_data();
            }); 
			
			get_data();
        }); 
		
		function get_data(){
			$.get('/get_data',function(data){
				$('#table-data').html(data);
			});
		}
	</script>
</html>
